# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

MAJOR version: when you make incompatible API changes.

MINOR version: when you add functionality in a backwards-compatible manner. 

PATCH version: when you make backwards-compatible bug fixes.

## [unreleased]
### Added
- .getUserFriendlyMessage(lang) to UTPError class
- language files

## [1.1.0] - June 13, 2018
### Added
- `UTPError` class, thrown when UTP service responds with a `Code` not equal to `00`


## [1.0.0] - June 12, 2018
### Changed
- Removed SoapService.js.
- Using `npm soap` module to handle soap requests/responses.
- Module is now initialized with .init(endpoint) method.


## [0.2.0] - June 9, 2018
### Changed
- SoapService.js -> soap.js
- xmlns is now added automatically, no need to specify it in the constructor.


## [0.1.1] - June 9, 2018
### Changed
- index.js -> example.js


## [0.1.0] - June 9, 2018
- Support for cookies, so api can remember http client in order to getBalance, buy, etc...
- Library uses promises
