# UTP Errors
**Notes** 

- Sometimes, UTP doesn't output propietary errors, instead uses carrier's own error messages (Claro).
- "Made up number" errors are the same as "wrong carrier" errors.

## Error: made up number
```js
utp.buy({
    reference: '4',
    productId: "357", // Claro
    amount: 25, 
    phone: "8091234567", // madeup number
})
```
Output:
```
Login { Code: '00', Message: 'Success' }
GetBalance { Code: '00', Message: 'Success', Balance: '362.25' }
Buy { Code: 'KO',
  Message: 'NUMERO TELEFONO ERRONEO',
  Details: '',
  Id: '256922027',
  TransactionId: '256922027',
  TransactionDateTime: '6/14/2018 8:22:00 PM',
  AuthorizationNumber: '' }
Logout { Code: '00', Message: 'Success' }
```

## Error: wrong carrier
----------------------------------------------------------------
### Claro
```js
utp.buy({
    reference: '10',
    productId: "357", // Claro
    amount: 50,
    phone: "8297265468", // Altice phone
}, 100)
```
```
Login { Code: '00', Message: 'Success' }
GetBalance { Code: '00', Message: 'Success', Balance: '362.25' }
Buy { Code: 'KO',
  Message: 'NUMERO TELEFONO ERRONEO',
  Details: '',
  Id: '256929113',
  TransactionId: '256929113',
  TransactionDateTime: '6/14/2018 9:15:06 PM',
  AuthorizationNumber: '' }
Logout { Code: '00', Message: 'Success' }
```
-----------------------------------------------------------------------
### Altice
```js
utp.buy({
    reference: '5',
    productId: "1743", // Altice
    amount: 30,
    phone: "8297716408", // Claro phone
})
```
```
Login { Code: '00', Message: 'Success' }
GetBalance { Code: '00', Message: 'Success', Balance: '362.25' }
Buy { Code: '191',
  Message: 'PROVIDER PLATFORM ERROR',
  Details: '',
  Id: '256924194',
  TransactionId: '256924194' }
Logout { Code: '00', Message: 'Success' }
```
---------------------------------------------------------------------
### Viva
```js
utp.buy({
    reference: '8',
    productId: "2501", // Viva
    amount: 50,
    phone: "8297716408", // Claro phone
}, 100)
```
```
Login { Code: '00', Message: 'Success' }
GetBalance { Code: '00', Message: 'Success', Balance: '362.25' }
Buy { Code: 'AC10',
  Message: 'TECHNICAL ERROR',
  Details: '',
  Id: '256926948',
  TransactionId: '256926948' }
Logout { Code: '00', Message: 'Success' }
```
-----------------------------------------------------------
### Tricom Altice
```js
utp.buy({
    reference: '13',
    productId: "2234", // Tricom Altice
    amount: 50,
    phone: "8297716408", // claro phone
}, 100)
```
```
Login { Code: '00', Message: 'Success' }
GetBalance { Code: '00', Message: 'Success', Balance: '267.25' }
Buy { Code: '01',
  Message: 'Numero Invalido o no Prepago.',
  Details: '',
  Id: '256932478',
  TransactionId: '256932478' }
Logout { Code: '00', Message: 'Success' }
GetBalance { Code: '00', Message: 'Success', Balance: '267.25' }
```
----------------------------------------------------------------------
### Digicel
```js
utp.buy({
    reference: '9',
    productId: "121", // digicel
    amount: 50,
    phone: "8297716408", // Claro phone
}, 100)
```
```
Login { Code: '00', Message: 'Success' }
GetBalance { Code: '00', Message: 'Success', Balance: '362.25' }
Buy { Code: 'AC10',
  Message: 'TECHNICAL ERROR',
  Details: '',
  Id: '256928586',
  TransactionId: '256928586' }
Logout { Code: '00', Message: 'Success' }
```
-------------------------------------------------------------------
### Natcom
```js
utp.buy({
    reference: '7',
    productId: "182", // Natcom
    amount: 50,
    phone: "8297716408", // Claro phone
}, 100)
```
```
Login { Code: '00', Message: 'Success' }
GetBalance { Code: '00', Message: 'Success', Balance: '362.25' }
Buy { Code: 'PA4',
  Message: 'UTP PRODUCT ID REQUIRE',
  Details: '',
  Id: '256926376',
  TransactionId: '256926376' }
Logout { Code: '00', Message: 'Success' }
```