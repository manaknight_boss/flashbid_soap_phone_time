# phone-time.js
Communicates to the SOAP web service of UTP to buy phone minutes for specified phone numbers.

## Installation
-----------------------------------------
`npm install`


## How to Use
-----------------------------------------
phone-time-example.js

```JavaScript
// 1. Require the module
var pt = require('./phone-time')
// url of SOAP service 
var endpoint = 'http://23.21.194.70:9191/Service.asmx?wsdl';
// parameters of the Login SOAP function, names must correspond to the xml tags
var loginArgs = {
    username: 'FLASHBIDUSR',
    password: 'K#Oxprcu9j',
    accountId: '1502',
};
// 2. Initialize
pt.init(endpoint).then(utp => {
    // It will provide an object of class UTP.
    // Inside this callback you may call the functions you need.
    // All functions return a Promise
    utp.login(loginArgs).then(result => {
        
        console.log(result)
        return utp.getBalance()

    }).then(balance => {
        
        console.log(result)
        return utp.logout()

    }).then(logoutResult => {
        console.log(logoutResult)
    })
})
```

### UTP class
---------------------------------------
#### .action(name, args)
Performs a SOAP function call, `name` is the SOAP function name and `args` is an object with the arguments the function accepts. This is the base method of the class, all others use it to execute SOAP functions. This means that if the UTP class doesn't have the method you need you can use this method directly.

**Notes:** 

- Names are case-sensitive. 

Example:
```javascript
utp.action('Buy', {
    reference: String,
    merchant: String, // not required 
    productId: String,
    amount: String, // or Number
    phone: String
}).then(result => console.log(result))
```

#### .login(args)
Login to the web service, `args` is the object with the arguments accepted by `Login` SOAP function.

**Notes:** 

- Names are case-sensitive. 

```xml
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
    <soap:Body>
        <Login xmlns="http://tempuri.org/">
            <username>string</username>
            <password>string</password>
            <accountId>string</accountId>
        </Login>
    </soap:Body>
</soap:Envelope>
```
```javascript
utp.login({
    username: String,
    password: String,
    accountId: String,
}).then(result => console.log(result))
```

#### .logout()
Logout from the web service

```js
utp.logout().then(...)
```

#### .getBalance()
Provides a number representing the balance on the account. 

```js
utp.getBalance().then(balance => console.log(balance))
```

**Notes:** 

- `.login()` must be called first.

#### .buy(args, minBalance)
Buys phone time for an specified phone number. It calls `.getBalance()` and compares the result with `minBalance`, throws an error when balance in the account is less than or equal to `minBalance`.

**Notes:** 

- Names are case-sensitive.
- `.login()` must be called first.
- You must provide a unique `reference` number and cannot contain letters.

```xml
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
    <soap:Body>
        <Buy xmlns="http://tempuri.org/">
            <reference>string</reference>
            <merchant>string</merchant>
            <productId>string</productId>
            <amount>string</amount>
            <phone>string</phone>
        </Buy>
    </soap:Body>
</soap:Envelope>
```
```javascript
utp.buy({
    reference: String,
    merchant: String, // not required 
    productId: String,
    amount: String, // or Number
    phone: String
}, Number).then(result => console.log(result))
```

### Errors
------------------------------------------------
Any error can be caught with the `.catch()` method. When UTP service returns a `*Result` object with code `00` the operation is considered succesful, any other code and the `.action()` method will throw an `UTPError` object with `code` and `message` properties, available with `.catch()`

To get a translated and user friendly error message use the `.getUserFriendlyMessage(lang)` method. The lang argument corresponds to a file in the `lang` directory.

Example:

You call the `.buy()` function with a bad argument:
```js
/* ... login then ... */

utp.buy({
    reference: "987647650",
    productId: "357",
    amount: 10, // amount is less than minimum accepted by the carrier
    phone: "8297716408"
})

/* ... then logout ... */
```
The response will be this:
```
Login { Code: '00', Message: 'Success' }
Buy { Code: 'DB2',
  Message: 'UTP INVALID PRODUCT OR AMOUNT FOR THIS ACCOUNT',
  Details: '',
  Id: '',
  TransactionDateTime: '',
  AuthorizationNumber: '',
  PinNumber: '',
  SerialNumber: '' }
Logout { Code: '00', Message: 'Success' }
```
`Code` is not `00` so it will throw an `UTPError`:
```js
// ... all .then() callbacks later...

.catch(UTPError => {
    // These are all available properties and methods
    console.log('code', UTPError.code)
    console.log('message', UTPError.message)
    console.log('details', UTPError.details)
    console.log('result', UTPError.result)
    console.log('nice message', UTPError.getUserFriendlyMessage('es'))
```
```

## Testing
--------------------------------------------
Using [Jest](https://facebook.github.io/jest/) as testing framework:

`npm run test`