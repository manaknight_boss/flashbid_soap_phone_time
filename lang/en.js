module.exports = {
    'RECHARGE_SUCCESS': 'Your Transaction has been successfully completed',
    'RECHARGE_TECH_ERROR': 'We can\'t process your transaction at this moment, try again later',
    'RECHARGE_USER_ERROR': 'We couldn\'t complete your transaction, make sure your provider and number are correct',
}