module.exports = {
    'RECHARGE_SUCCESS': 'Tu transacción fue completada exitosamente',
    'RECHARGE_TECH_ERROR': 'En estos momentos no podemos procesar tu transacción, inténtalo de nuevo más tarde',
    'RECHARGE_USER_ERROR': 'Tu Transacción no pudo ser completada, asegura que tu proveedor y número sean correctos',
}