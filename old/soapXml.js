var builder = require('xmlbuilder');
var request = require('request-promise');
var parseXml = require('xml2js').parseString;
var Promise = require('bluebird');

// enable this to show all http requests in console
// require('request-debug')(request);

class Soap {

    constructor(config) {
        // url to access
        this.endpoint = config.endpoint;
        
    }

    /**
     * Creates the xml for the envelope
     * 
     * @param {string} parent Name of the SOAP function will become a node inside <soap:Body>
     * @param {object} childs The func. parameters will become nodes inside, ie: <Login><username>...
     * @returns {string} XML formatted string
     */
    createEnvelope (parent, childs) {
        return builder.create({
            'soap:Envelope': {
                '@xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
                '@xmlns:xsd': "http://www.w3.org/2001/XMLSchema",
                '@xmlns:soap': 'http://schemas.xmlsoap.org/soap/envelope/',
                
                "soap:Body":  () => {
                    var nodes = {};
                    
                    nodes[parent] = {
                        "@xmlns" : "http://tempuri.org/",
                    };
    
                    Object.assign(nodes[parent], childs || {});
    
                    return nodes;
                    
                }
            }
        }, { encoding: 'utf-8'}).end({
            pretty: true
        });
    }
    
    /**
     * Creates the SOAP envelope and sends it to the endpoint
     * 
     * @param {string} action Name of the soap action to call
     * @param {object} parameters Accepted by the function, names must be the same as the xml tags
     * @returns {Promise}
     */
    requestSoap (action, parameters) {
        var envelope = this.createEnvelope(action, parameters);
        
        return request({
            method: 'POST',
            
            uri: this.endpoint,
            
            // commented headers don't seem to be needed
            headers: {
                "Content-Type" : "text/xml; charset=utf-8",
                // "Host" : "localhost",
                // "SOAPAction" : `${this.actionUrl}${action}`,
            },

            jar: true, // enable cookies, saves the login cookie so api can remember our user

            body: envelope,
            
        }).then((response)=>{
            // when xml arrives
            return new Promise((resolve, reject)=>{
                // Parse the received xml to an object
                parseXml(response, {explicitArray:false, ignoreAttrs:true}, (error, result) => {
                    if (error) {
                        reject(error.message)
                    }
                    /* 
                        this is the expected format:
                        <soap:Envelope>
                            <soap:Body>
                                <[ACTION]Response>
                                    <[ACTION]Result>
                                        .....
                                    </[ACTION]Result>
                                </[ACTION]Response>
                            </soap:Body>
                        </soap:Envelope>
                     */
                    // TODO: verify the response format before proceding
                    var data = result['soap:Envelope']['soap:Body'][`${action}Response`][`${action}Result`];
                    
                    // Calls will always include these tags: <Code> and <Message>
                    // inside the <[ACTION]Result>
                    // "00" is the code for success, anything else means the api couldn't perform the action
                    // and will return another code ie: AC9
                    if (data.Code !== "00")  {

                        reject(data.Message);
                    }
                    else {
                        
                        resolve(data);
                    }
    
                });
            })

            
        })
      
    }
}


module.exports = {
    create: config => new Soap(config),
}