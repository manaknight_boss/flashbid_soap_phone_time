// 1. Require the module
var pt = require('./phone-time')

// Url of SOAP service 
var endpoint = 'http://23.21.194.70:9191/Service.asmx?wsdl';

// Parameters of the Login SOAP function, names must correspond to the xml tags
var loginArgs = {
    username: 'FLASHBIDUSR',
    password: 'K#Oxprcu9j',
    accountId: '1502',
};

// 2. Initialize
pt.init(endpoint).then(utp => {
    // It will provide an object of class UTP.
    // Inside this callback you may call the functions you need.
    // All functions return a Promise

    // 3. Use the functions
    utp.login(loginArgs)
    .then(result => 

         utp.buy({
            reference: '13',
            productId: "2234", // Tricom Altice
            amount: 50,
            phone: "8297716408", // claro phone
        }, 100)

    ).then(result => {
        // do something with result

    }).catch(error => {
        // do something with error
        console.log(error.getUserFriendlyMessage('es'))

    }).finally(function() {
        utp.getBalance()
        .then(
            r => utp.logout()
        ) 
    })
})
