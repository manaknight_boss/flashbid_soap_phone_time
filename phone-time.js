class UTPError extends Error {
    constructor(actionResult) {
        super(actionResult.Message)
        // remove constructor from stack trace, only exists in node.
        Error.captureStackTrace(this, UTPError)

        this.code = actionResult.Code;
        this.details = actionResult.Details;
        this.result = actionResult;
    }

    getUserFriendlyMessage(lang) {
        var lang = require(`./lang/${lang}`)
        switch(this.code) {
            case 'KO' :
            case '191' :
            case 'AC10' :
            case '01' :
            case '121' :
            case 'PA4' :
                return lang.RECHARGE_USER_ERROR;

            default:
                return lang.RECHARGE_TECH_ERROR;

            
        }
    }
}

class UTP {

    /**
     * Constructor
     * 
     * @param {Client} soapClient 
     */
    constructor(soapClient) {
        this.soap = soapClient;
    }

    /**
     * Execute a soap function and provides an [action]Result object
     * @param {string} name Name of the SOAP function 
     * @param {object} args Soap function parameters
     */
    action (name, args) {
        return this.soap[`${name}Async`](args)
        .then(result => {
            let actionResult = result[0][`${name}Result`]

            // return data;
            console.log(name, actionResult)

            if (actionResult.Code !== '00') {
                throw new UTPError(actionResult)
            } else {
                return actionResult;
            }

        });
    }

    /**
     * Login to the SOAP API server
     */
    login (args) {
        return this.action('Login', args);
    }

    /**
     * Logout from the SOAP API
     */
    logout () {
        return this.action('Logout');
    }

    /**
     * Gets the balance of the account 
     */
    getBalance() { 
       return this.action("GetBalance").then(data => {
            return data.Balance;
       });
    }

    /**
     * Buys phone time if balance is over minBalance
     * @param {object} info Required info to buy, corresponds to the SOAP xml
     * @param {number} minBalance Threshold of minimum balance to prevent buying
     */
    buy (args, minBalance) {
       return this.getBalance().then(balance => {

           if (balance <= minBalance) {

               throw new Error("Account balance is close to minimum");

           } else {

               return this.action('Buy', args);
           }

       }) 
    }

}

class PhoneTime {
    constructor(soap, request) {
        this.soap = soap
        this.request = request
    }
    
    init (endpoint) {
    
        return this.soap.createClientAsync(endpoint, { request : this.request })
        .then(soapClient => {
            return new UTP(soapClient);
        })
    }



}


module.exports = {
    UTP: UTP,
    PhoneTime: PhoneTime,

    init(endpoint) {

        var phoneTime = new PhoneTime(
            require('soap'),
            require('request').defaults({ jar: true, }) // enable cookies, so the web service can remember our user.
        )

        return phoneTime.init(endpoint)
    }

};