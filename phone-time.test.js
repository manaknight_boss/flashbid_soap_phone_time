var Promise = require('bluebird');
var pt = require('./phone-time')
var langEs = require('./lang/es')

// mock soapClient
var soapClient = {
            
    GetBalanceAsync: jest.fn().mockImplementation(args => {
        return Promise.resolve(
            [{ GetBalanceResult: { Code:"00", Message:"Success", Balance:0 } }]
        );
    }),

    BuyAsync: jest.fn().mockImplementation(args => {
        return Promise.resolve(
            [{ BuyResult: { Code:"KO", Message:"NUMERO TELEFONO ERRONEO",  } }]
        )
    })

}

var utp = new pt.UTP(soapClient)

/**
 * ---------------------------------------------------------
 * ********************** TESTS ****************************
 * ---------------------------------------------------------
 */
beforeEach(function() {
    jest.clearAllMocks();
})

describe('UTP', function() {
    describe('.action(action, args)', () => {

        it('should provide a Result object with Code and Message', () => {

            return utp.action('GetBalance').then(GetBalanceResult => {

                expect(GetBalanceResult).toMatchObject({
                    Code: expect.any(String),
                    Message: expect.any(String)
                })
            })
        })

        it('should call the right soap function on the client', () => {
            utp.action('GetBalance')
            utp.action('GetBalance')
            expect(soapClient.GetBalanceAsync.mock.calls.length).toBe(2)
        })

    })

    describe('.getBalance()', function() {
        it('should provide a number greater than or equal to zero', function () {
            return utp.getBalance().then(function(balance) {
                expect(balance).toBeGreaterThanOrEqual(0)
            })
        })
    })

    
})

describe('UTPError', function () {
    describe('.getUserFrienflyMessage()', function() {
        it('should return the correct translated error message', function() {
            return utp.action('Buy').catch(error => {
                expect(error.getUserFriendlyMessage('es')).toBe(langEs.RECHARGE_USER_ERROR)
            })
        })
    })
})